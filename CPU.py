from Cache import Cache
from MainMemory import MainMemory
from cmyk import CMYK as Color


class CPU:
    def __init__(self, main_memory: MainMemory, debug=False):
        self.hits = 0
        self.total_accesses = 0
        self.debug = debug

        self.main_memory = main_memory
        self.cache = Cache(self.cache_size_init(), len(Color.color_parts), debug)

    def cache_size_init(self, cache_size=None):
        max_cache_size = int(len(self.main_memory.memory) * len(self.main_memory.memory[0]) / 4)
        max_cache_size = max_cache_size - max_cache_size % 2

        if cache_size is None:
            cache_size = int(input('Cache size (max: {}) : '.format(max_cache_size)).strip())
            if not ((cache_size & (cache_size - 1)) == 0) and cache_size > 0:  # pow of 2 check
                print('invalid cache size')
                exit(1)
        if cache_size == 2:  # todo temp
            cache_size = 0

        def default_init():
            print('initializing with max size: {}'.format(max_cache_size))
            return max_cache_size

        if cache_size % 2 != 0:
            print('! invalid cache size !')
            cache_size = default_init()
        elif cache_size > max_cache_size:
            print('! cache is more than main memory !')
            cache_size = default_init()

        return cache_size

    def reset_stats(self):
        self.hits = 0
        self.total_accesses = 0

    def start_logging(self):
        print('Logging started')
        self.reset_stats()

    def end_logging(self):
        print('Logging end')
        self.get_logging_info()
        self.reset_stats()

    def get_logging_info(self):
        print('Total accesses: {}'.format(self.total_accesses))
        print('Hits: {}'.format(self.hits))
        miss_count = self.total_accesses - self.hits
        print('Misses: {}'.format(miss_count))
        print('Hit/miss rate: {}'.format(self.hits / self.total_accesses))

    def get(self, row: int, col: int, color_part_code: chr):
        # check in cache
        address = self.main_memory.get_address(row, col)
        value = self.cache.get(address, Color.color_parts.index(color_part_code))
        self.total_accesses += 1

        if value is None:  # check in mem
            self.update_cache([row, col])
            color: Color = (self.main_memory.memory[row])[col]
            return color.get(color_part_code)
        self.hits += 1
        return value

    def set(self, row: int, col: int, color_part_code: chr, value: int):  # todo
        # write to cache
        address = self.main_memory.get_address(row, col)
        is_cached = self.cache.set(value, address, Color.color_parts.index(color_part_code))
        self.total_accesses += 1

        if not is_cached:  # check in mem
            self.write_from_cache()
            self.update_cache([row, col])
            self.cache.set(value, address, Color.color_parts.index(color_part_code))
            color: Color = ((self.main_memory.memory[row])[col])
            color.set(color_part_code, value)
            (self.main_memory.memory[row])[col] = color
        else:
            self.hits += 1

    def update_cache(self, start_address: list):
        self.cache.empty_cache()
        new_cache = []
        new_tags = []
        last_offset = self.cache.last_offset

        if last_offset > 0:  # todo deal with cache_size % 4 != 0
            last_color: Color = (self.main_memory.memory[start_address[0]])[start_address[1]]
            for index in range(last_offset, 4):
                new_cache.append(last_color.as_list()[index])
            self.cache.last_offset = 3 - last_offset
            print('from prev cache: ', new_cache, 'new last offset: ', self.cache.last_offset)

        for row_index in range(start_address[0], self.main_memory.size[0]):
            for col_index in range(start_address[1], self.main_memory.size[1]):
                if len(new_cache) < self.cache.cache_size:
                    color: Color = (self.main_memory.memory[row_index])[col_index]
                    new_cache.extend(color.as_list())
                    new_tags.append(self.main_memory.get_address(row_index, col_index))
                else:
                    break
        self.cache.update_cache(new_cache, new_tags)

    def write_from_cache(self):
        cache = self.cache.memory
        if len(self.cache.tags) > 0:
            for each in self.cache.tags:
                m, n = self.main_memory.get_indexes(each)
                (self.main_memory.memory[m])[n] = Color(cache[0:4])
                cache = cache[4:]

    def stop_writing(self):
        self.write_from_cache()
        self.cache.empty_cache()

    def print_main_memory(self):
        print(self.main_memory)
        for row in range(self.main_memory.size[0]):
            line = ''
            for col in range(self.main_memory.size[1]):
                # print((self.main_memory.memory[row])[col].as_list())
                line += '  #{list[0]:^3}/{list[1]:^3}/{list[2]:^3}/{list[3]:^3}  |'.format(
                    list=(self.main_memory.memory[row])[col].as_list())
            print(line)

    def print_cache_memory(self):
        pass
