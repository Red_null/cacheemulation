class Cache:
    def __init__(self, cache_size, block_size, debug=False):
        self.debug = debug
        self.memory = [None] * cache_size
        self.cache_size = cache_size
        self.block_size = block_size
        self.tags = []
        self.last_offset = 0

    def get(self, address, offset):
        if address in self.tags:
            index = self.tags.index(address)
        else:
            return None
        return self.memory[index + offset]

    def set(self, value, address, offset):
        if self.debug:
            print(self.memory, self.tags, address, offset)
        if address in self.tags:
            index = self.tags.index(address) * 4
            self.memory[index + offset] = value
            return True
        else:
            return False

    def empty_cache(self):
        self.memory = [None] * self.cache_size
        self.last_offset = 0

    def update_cache(self, new_cache, tags):
        self.memory = new_cache
        self.tags = tags

    def __repr__(self):
        return str(self.memory)
