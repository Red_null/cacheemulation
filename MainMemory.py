class MainMemory:
    def __init__(self, data_array):
        self.memory = data_array
        self.size = [len(data_array), len(data_array[0])]

    def get(self, value):
        print(value)
        for row in self.memory:
            for each in row:
                if each == value:
                    return each
        return None

    def get_address(self, row, col):
        return hex(self.size[1] * row + col)

    def get_indexes(self, address):
        address = int(address, 16)
        row = address // (self.size[1])
        col = address - (self.size[1]) * row

        return row, col

    def __repr__(self):
        return str(self.memory)
