class CMYK:
    color_parts = ('c', 'm', 'y', 'k')

    def __init__(self, color_arr):
        self.c = color_arr[0]
        self.m = color_arr[1]
        self.y = color_arr[2]
        self.k = color_arr[3]
        self.color_map: dict = {'c': self.c, 'm': self.m, 'y': self.y, 'k': self.k}

    def get(self, color_part_code: chr):
        if color_part_code not in self.color_parts:
            print('invalid color part index')
            return None
        return self.color_map.get(color_part_code)

    def set(self, color_part_code: chr, value):
        if color_part_code not in self.color_parts:
            print('invalid color part index')
            return None
        else:
            exec('self.{}={}'.format(color_part_code, value))

    def as_list(self):
        return [self.c, self.m, self.y, self.k]

    def __str__(self):
        return '#{}|{}|{}|{}'.format(self.c, self.m, self.y, self.k)

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        if self.c == other.c and self.m == other.m and self.y == other.y and self.k == other.k:
            return True
        else:
            return False
