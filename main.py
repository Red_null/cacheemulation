"""
Luboshnikov Anton is1609
"""
from random import Random

from CPU import CPU
from MainMemory import MainMemory
from cmyk import CMYK


def cpu_init(pre_defined_array=False, debug=False):
    m, n = get_memory_size()

    if pre_defined_array is False:
        data = get_user_colors(m, n)
    else:
        data = get_random_colors(m, n)

    simulated_cpu = CPU(MainMemory(data), debug)
    simulated_cpu.print_main_memory()
    return simulated_cpu


def get_memory_size():
    m, n = map(int, input('Enter memory size as mxn: ').strip().split('x'))
    if m % 2 != 0 or n % 2 != 0:
        print('invalid memory size: must be power of 2')
        get_memory_size()
        return
    return m, n


def get_user_colors(m, n):
    memory_arr = []

    for row in range(m):
        memory_arr.append([])
        for column in range(n):
            new_color = list(map(int, input('Color as c/m/y/k: ').strip().split('/')))
            memory_arr[row].append(new_color)
    return memory_arr


def get_random_colors(m, n):
    randomizer = Random()
    memory_arr = []
    for row in range(m):
        memory_arr.append([])
        for col in range(n):
            color_arr = []
            for color_part in range(4):
                color_arr.append(randomizer.randint(0, 255))
            memory_arr[row].append(CMYK(color_arr))
    return memory_arr


def alg1():
    print('alg1')
    for row in range(cpu.main_memory.size[0]):
        for col in range(cpu.main_memory.size[1]):
            cpu.set(row, col, 'c', 0)
            cpu.set(row, col, 'm', 0)
            cpu.set(row, col, 'y', 1)
            cpu.set(row, col, 'k', 0)
    cpu.stop_writing()
    cpu.print_main_memory()
    print(cpu.cache.memory)
    print('alg1 end')


def alg2():
    print('alg 2')
    for col in range(cpu.main_memory.size[1]):
        for row in range(cpu.main_memory.size[0]):
            cpu.set(row, col, 'c', 0)
            cpu.set(row, col, 'm', 0)
            cpu.set(row, col, 'y', 1)
            cpu.set(row, col, 'k', 0)
    cpu.stop_writing()
    cpu.print_main_memory()
    print(cpu.cache.memory)
    print('alg2 end')


def alg3():
    print('alg 3')
    for row in range(cpu.main_memory.size[0]):
        line = ''
        for col in range(cpu.main_memory.size[1]):
            cpu.set(row, col, 'c', 0)
            cpu.set(row, col, 'm', 0)
            cpu.set(row, col, 'k', 0)

    for row in range(cpu.main_memory.size[0]):
        line = ''
        for col in range(cpu.main_memory.size[1]):
            cpu.set(row, col, 'y', 1)
    cpu.stop_writing()
    cpu.print_main_memory()
    print(cpu.cache.memory)
    print('alg3 end')


if __name__ == '__main__':
    cpu = cpu_init(True,True)  # True to generate numbers; True to debug
    cpu.start_logging()
    alg1()
    cpu.end_logging()

    cpu = cpu_init(True,True)  # True to generate numbers; True to debug
    cpu.start_logging()
    alg2()
    cpu.end_logging()

    cpu = cpu_init(True,True)  # True to generate numbers; True to debug
    cpu.start_logging()
    alg3()
    cpu.end_logging()
